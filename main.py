from __future__ import print_function
import sys, os
MAIN_FILE_NAME = os.path.dirname(os.path.realpath(__file__))
MODULE_LOCATION = os.path.split(MAIN_FILE_NAME)[0]
sys.path.append(MODULE_LOCATION)
from python_experiments.java_exp import run_java_exp
__author__ = 'dso'


if __name__ == "__main__":
    if len(sys.argv) < 7:
        print (sys.argv[0] + \
            "JavaClientExperiment <host> <log_dir_location> <log_filename> <memory_pressure_factor>"+\
            " <lifetime_factor>, <request_factor>, <concurrent_session_factor> [destroy_password]\n"+\
            "Allowed factor values are : VLOW, LOW, MED, HIGH, VHIGH\n")
        print ("====ERROR====", file=sys.stderr)
        sys.exit(-1)
    run_java_exp(*(sys.argv[1:]))

