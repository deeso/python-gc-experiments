from __future__ import print_function
__author__ = 'dso'

from datetime import datetime
from param_gen import ParameterGeneration, MEMORY_WATER_MARK, MAX_MEMORY
from exp_session import ExperimentSession

from traceback import print_exc
import threading, sys, time, os, ssl
import gc
from multiprocessing import Queue

MAX_EXPERIMENT_TIME = 420
AMAX_EXPERIMENT_TIME = 420
EXPERIMENT_TIME = 240
LOG_DATA_ENTRY = 0

MAX_SESSION_LIFETIME = EXPERIMENT_TIME
MAX_ALLOWED_REQ = 120
MAX_CONCURRENT_SESSIONS = 20 
#USED_MEM = (int) (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory())
DATE_FORMAT = "yyyy.MM.dd_HH.mm.ss"

dummy = False

VHIGH = 1
HIGH = 2
MED = 3
LOW = 4
VLOW = 5
RANDOM = 6

VHIGH_FACTOR = .8
HIGH_FACTOR = .6
MED_FACTOR = .4
LOW_FACTOR = .1
VLOW_FACTOR = .01
RANDOM_FACTOR = -1.0
VHIGH_FACTOR_STRING ="VHIGH"
HIGH_FACTOR_STRING ="HIGH"
MED_FACTOR_STRING ="MED"
LOW_FACTOR_STRING ="LOW"
VLOW_FACTOR_STRING ="VLOW"
RANDOM_FACTOR_STRING ="RANDOM"

USE_FACTOR = {
    VHIGH:VHIGH_FACTOR,
    HIGH:HIGH_FACTOR,
    MED:MED_FACTOR,
    LOW:LOW_FACTOR,
    VLOW:VLOW_FACTOR,
    RANDOM:RANDOM_FACTOR,

    VHIGH_FACTOR:VHIGH,
    HIGH_FACTOR:HIGH,
    MED_FACTOR:MED,
    LOW_FACTOR:LOW,
    VLOW_FACTOR:VLOW,
    RANDOM_FACTOR:RANDOM,

    VHIGH_FACTOR_STRING:VHIGH,
    HIGH_FACTOR_STRING:HIGH,
    MED_FACTOR_STRING:MED,
    LOW_FACTOR_STRING:LOW,
    VLOW_FACTOR_STRING:VLOW,
    RANDOM_FACTOR_STRING:RANDOM,
}

EXP_NOT_STARTED = 0
EXP_RUNNING = 1
EXP_COMPLETE = 2


class SignalCleanup(object):
    def __init__(self, JavaClientExp):
        self.javaExp = JavaClientExp

    def run(self):
        self.javaExp.myCurrentState = EXP_COMPLETE
        self.javaExp = None


class MonitorSession(threading.Thread):

    def __init__(self, JavaClientExp):
        threading.Thread.__init__(self)
        self.javaExp = JavaClientExp
        self.standardSleepTime = 1.0

    def run(self):
        self.javaExp.running = True
        self.standardSleepTime = 1.0
        self.startTime = ParameterGeneration.getinstance().getTime()

        self.endTime = self.startTime + MAX_EXPERIMENT_TIME
        #signalTheEnd.schedule(new SignalCleanup(), new Date(endTime*1000))
        while self.javaExp.keepRunning and self.javaExp.myCurrentState != EXP_COMPLETE:
            try:
                while self.javaExp.canCreateNewSession():
                    self.javaExp.createNewClientSession()
                    if self.javaExp.myCurrentState == EXP_COMPLETE:
                        break
                    if self.endTime <= ParameterGeneration.getinstance().getTime():
                        self.javaExp.myCurrentState = EXP_COMPLETE
                        break

                time.sleep(self.standardSleepTime)
            except Exception, ex:
                self.javaExp.stdout("Interrupt has occurred, must need to start a client")
                if self.javaExp.myCurrentState == EXP_COMPLETE:
                    break
                print_exc()
                out = "Error: JavaClientExperiments "+str(ex)
                self.javaExp.stdout(out)
        self.javaExp.performCleanup()



class JavaClientExperiments(object):
    gJavaClientExp = None
    base_size = None
    @classmethod
    def getExp(cls):
        return cls.gJavaClientExp

    def __init__(self, host, location, memPressureFactor, sessLifetimeFactor, reqAllowedFactor, numsessFactor, destroy_pass=False, use_tlslite=True, perform_as_loop=False):
        gc.enable()
        #ssl._create_default_https_context = ssl._create_unverified_context
        self.sessLifetime = 0
        self.allowedSessions = 0
        self.allowedRequests = 0
        self.memoryPerSession = 0
        self.myCurrentState = EXP_NOT_STARTED
        #Runtime runtime = Runtime.getRuntime()
        self.livingSessions = {}
        self.host = ""
        self.UserAuthLogFile = None
        self.destroy_pass = False
        self.logfilename = None
        self.maxMemoryConsumption = 0
        self.startTime = 0
        self.numSessionsCreated = 0
        self.monitorSessions = None
        self.signalTheEnd = None
        self.keepRunning = True
        self.endTime = 0
        self.startCleaningUp = None
        self.running = False
        self.USE_RANDOM_SESS_LIFETIME = False
        self.USE_RANDOM_MEM_PER_SESSION = False
        self.USE_RANDOM_ALLOWED_SESS = False
        self.USE_RANDOM_CONC_SESSIONS = False
        self.USE_RANDOM_ALLOWED_REQ = False
        self.memPressureFactor = 0.0
        #self.dummy = threading.RLock()
        #self.UserAuthLogFileLock = threading.RLock()
        self.use_tlslite = use_tlslite
        self.out_queue = Queue()
        self.auth_queue = Queue()
        self.init_from(host, location, memPressureFactor, sessLifetimeFactor, reqAllowedFactor, numsessFactor)
        self.printer = threading.Thread(target=self.print_out, args=())
        self.auth_file_writer = threading.Thread(target=self.auth_log_out, args=())
        self.perform_as_loop = perform_as_loop

    def useTlslite(self):
        return self.use_tlslite

    def setTlslite(self, v):
        self.use_tlslite = v

    def stdout(self, msg):
        #self.dummy.acquire()
        #print(msg)
        #self.dummy.release()
        msg = msg if msg[-1] == '\n' else msg+'\n'
        self.out_queue.put(msg)

    def print_out(self):
        while self.running:
            if self.out_queue.qsize() > 0:
                msg = self.out_queue.get()
                sys.stdout.write(msg)
                sys.stdout.flush()
            else:
                time.sleep(.5)

    def setRunning(self, b):
        self.running = b

    def isRunning(self):
        return self.running

    def getLiveSessions(self):
        return len(self.livingSessions)

    def getRemainingTime(self):
        return self.end - ParameterGeneration.getinstance().getTime()

    def canCreateSessions(self):
        return True

    def canCreateSessions(self):
        return self.getLiveSessions() < self.getAllowedSessions() \
               and self.canCreateNewSessInRemaingTime()

    def start(self):
        self.running = True
        if not self.monitorSessions is None and self.monitorSessions.isAlive():
            return
        self.printer.start()
        self.auth_file_writer.start()
        self.myCurrentState = EXP_RUNNING
        self.monitorSessions = MonitorSession(self)
        self.monitorSessions.start()
        self.baseSize()

    def calculateSize(self):
        return self.baseSize() + sum([i.calculateSize() for i in self.livingSessions.values()])

    def baseSize(self):
        if self.base_size is None:
            self.base_size = ParameterGeneration.getinstance().getUsedMemory(self)
        return self.base_size

    def getMaxMemoryConsumption(self):
        return self.maxMemoryConsumption

    def getSessLifetime(self):
        if self.USE_RANDOM_SESS_LIFETIME or self.sessLifetime <= 0:
            t = ParameterGeneration.getinstance().randomDouble(LOW_FACTOR, HIGH_FACTOR)
            return self.self.doubleToLong(t*MAX_SESSION_LIFETIME)
        return self.sessLifetime

    def setSessLifetime(self, sessLifetime):
        self.sessLifetime = sessLifetime

    def getMemoryPressureFactor(self):
        f = len(self.livingSessions)
        if f == 0 and self.USE_RANDOM_MEM_PER_SESSION:
            return ParameterGeneration.getinstance().randomDouble(LOW_FACTOR, HIGH_FACTOR)
        elif f == 0:
            return self.memPressureFactor
        elif self.USE_RANDOM_MEM_PER_SESSION:
            return ParameterGeneration.getinstance().randomDouble(LOW_FACTOR, HIGH_FACTOR) / len(self.livingSessions)
        else:
            return f / len(self.livingSessions)

    def getMemoryPerSession(self):
        if self.USE_RANDOM_MEM_PER_SESSION or self.memoryPerSession <= 0:
            t = ParameterGeneration.getinstance().randomDouble(LOW_FACTOR, HIGH_FACTOR)
            return self.doubleToLong(t*MAX_MEMORY)
        return self.memoryPerSession

    def getAllowedSessions(self):
        return self.allowedSessions

    def getAllowedRequests(self):
        if self.USE_RANDOM_ALLOWED_SESS or self.allowedRequests <= 0:
            t = ParameterGeneration.getinstance().randomDouble(LOW_FACTOR, HIGH_FACTOR)
            return self.doubleToLong(t*MAX_ALLOWED_REQ)
        return self.allowedRequests

    def expNotify(self):
        #  {
        #     synchronized (myCurrentState) {
        #         if (myCurrentState == STATE.EXP_RUNNING)
        #             monitorSessions.notify()
        #     }
        # }
        pass

    def handleCompleteSession(self, sessionId):
        if sessionId in self.livingSessions:
            del self.livingSessions[sessionId]
        self.expNotify()

    def handleFailedSession(self, sessionId):
        if sessionId in self.livingSessions:
            del self.livingSessions[sessionId]
        self.expNotify()

    def createNewClientSession(self):
        esess = ExperimentSession(self)
        userid = esess.getExpID()
        self.livingSessions[userid] = esess
        esess.start()
        time_ = long(time.mktime(datetime.now().timetuple()))
        out = "Starting-Time:{:08x}-Session:{}".format(time_, userid)
        self.stdout(out)

    def isDestroy_pass(self):
        return self.destroy_pass

    def setDestroy_pass(self, destroy_pass=False):
        self.destroy_pass = destroy_pass

    def performCleanup (self):
        self.sessions = []
        #self.signalTheEnd.cancel()
        sess_keys = self.livingSessions.keys()
        self.stdout("Shutting down the sessions")
        for userid in sess_keys:
            exp_sess = self.livingSessions.pop(userid) if userid in self.livingSessions else None
            if not exp_sess is None and exp_sess.isAlive():
                exp_sess.setShutdown()
                self.sessions.append(exp_sess)
        time.sleep(.500)
        self.setRunning(False)

    def doubleToLong(self, v):
        return long(v)


    def getFactorValue(self, factor):
        if factor in USE_FACTOR:
            return USE_FACTOR[factor]
        return VLOW_FACTOR


    def getFactorValue(self, factor_string):
        factor = LOW_FACTOR
        if factor_string.upper() in USE_FACTOR:
            factor = USE_FACTOR[factor_string.upper()]

        return USE_FACTOR[factor]

    def allowedToAllocate(self):
        #return Runtime.getRuntime().maxMemory() - Runtime.getRuntime().freeMemory() < MIN_CUSHION
        return True


    def memoryPressureInRange(self):
        #return runtime.freeMemory() < maxMemoryConsumption
        return True

    def getTotalMemory(self):
        return ParameterGeneration.getinstance().allowedTotalMemory()

    def getFreeMemory(self):
        return ParameterGeneration.getinstance().getFreeMemory()

    def getUsedMemory(self):
        return ParameterGeneration.getinstance().getUsedMemory(self)

    def getTotalUsedMemory(self):
        return self.calculateSize()

    def calculateMemoryPerSession(self):
        if self.USE_RANDOM_MEM_PER_SESSION or self.memoryPerSession <= 0:
            return self.getMemoryPerSession()
        return self.maxMemoryConsumption / self.allowedSessions

    def canCreateNewSession(self):
            memPred = False
            sessPred = False
            if (self.USE_RANDOM_MEM_PER_SESSION or self.memoryPerSession <= 0) or\
                (self.USE_RANDOM_ALLOWED_SESS or self.allowedSessions <= 0):
                memPred = MEMORY_WATER_MARK > self.getTotalUsedMemory()
            else:
                memPred = self.maxMemoryConsumption > self.getTotalUsedMemory()

            if self.USE_RANDOM_ALLOWED_SESS or self.allowedSessions <= 0:
                sessPred = len(self.livingSessions) < self.MAX_CONCURRENT_SESSIONS
            else:
                sessPred = len(self.livingSessions) < self.allowedSessions
            return sessPred and memPred

    def getRemoteHost(self):
        return self.host

    def generateName(self ):
        return  self.logfilename


    def openLogFile(self):
        base_dir = os.path.split(self.logfilename)[0]
        try:
            os.stat(base_dir)
        except:
            try:
                os.makedirs(base_dir)
            except:
                pass
        self.UserAuthLogFile = open(self.logfilename, 'w')
    
    def auth_log_out(self):
        self.openLogFile()
        while self.running:
            if self.auth_queue.qsize() > 0:
                msg = self.auth_queue.get()
                self.UserAuthLogFile.write(msg)
                self.UserAuthLogFile.flush()
            else:
                time.sleep(.5)
                

    def logUserAuth(self, time_, user, pass_, sess):
        #self.UserAuthLogFileLock.acquire()
        msg = "{},{},{},{}\n".format(time_,user,pass_,sess)
        self.auth_queue.put(msg)
        #self.UserAuthLogFile.write(msg)
        #self.UserAuthLogFile.flush()
        #self.UserAuthLogFileLock.release()

    def init_from(self, host, location, memPressureFactor, sessLifetimeFactor, reqAllowedFactor, numsessFactor):
        self.maxMemoryConsumption = self.doubleToLong(self.getFactorValue(memPressureFactor) * MEMORY_WATER_MARK )
        self.memPressureFactor = self.getFactorValue(memPressureFactor)
        if self.maxMemoryConsumption < 0.0:
            self.USE_RANDOM_MEM_PER_SESSION = True

        self.sessLifetime = self.doubleToLong(self.getFactorValue(sessLifetimeFactor) * MAX_SESSION_LIFETIME)
        if self.sessLifetime < 0.0:
            self.USE_RANDOM_SESS_LIFETIME = True

        self.allowedRequests = self.doubleToLong(self.getFactorValue(reqAllowedFactor) * MAX_ALLOWED_REQ)
        if self.allowedRequests < 0.0:
            self.USE_RANDOM_ALLOWED_REQ = True

        self.allowedSessions = self.doubleToLong(self.getFactorValue(numsessFactor) * MAX_CONCURRENT_SESSIONS)
        if self.allowedSessions < 0.0:
            temp = ParameterGeneration.getinstance().randomDouble(LOW_FACTOR, HIGH_FACTOR)
            self.allowedSessions = self.doubleToLong(temp * MAX_CONCURRENT_SESSIONS)

        #memoryPerSession = calculateMemoryPerSession()
        self.host = host
        self.logfilename = location
        self.openLogFile()
        self.gJavaClientExp = self

def run_java_exp_ssl(*args):
    if len(args) < 6:
        print (
            "JavaClientExperiment <host> <log_dir_location> <log_filename> <memory_pressure_factor>"+\
            " <lifetime_factor>, <request_factor>, <concurrent_session_factor> [destroy_password]\n"+\
            "Allowed factor values are : VLOW, LOW, MED, HIGH, VHIGH\n")
        print ("====ERROR====", file=sys.stderr)
        sys.exit(-1)
    host = args[0]
    log_filename = args[1]
    memory_pressure_factor = args[2]
    lifetime_factor = args[3]
    request_factor = args[4]
    concurrent_session_factor = args[5]
    destroy_password = bool(args[6]) if len(args) > 6 else False
    use_tlslite= False


    jclientExp = JavaClientExperiments(host, log_filename, memory_pressure_factor, lifetime_factor, request_factor, concurrent_session_factor, perform_as_loop=True, use_tlslite=use_tlslite)
    jclientExp.setDestroy_pass(destroy_password)
    #jclientExp.stdout("Error: "+str(ex))
    jclientExp.start()
    while jclientExp.isRunning():
        try:
            time.sleep(.500)
        except MemoryError, oom_e:
            jclientExp.stdout("Error: "+str(oom_e))
        except Exception, e:
            jclientExp.stdout("Error: "+str(e))

    jclientExp.stdout("====DONE====")
    try:
        time.sleep(60)
    except MemoryError, oom_e:
        jclientExp.stdout("Error: "+str(oom_e))
    except Exception, e:
        jclientExp.stdout("Error: "+str(e))

    return 0

def run_java_exp_tlslite(*args):
    if len(args) < 6:
        print (
            "JavaClientExperiment <host> <log_dir_location> <log_filename> <memory_pressure_factor>"+\
            " <lifetime_factor>, <request_factor>, <concurrent_session_factor> [destroy_password]\n"+\
            "Allowed factor values are : VLOW, LOW, MED, HIGH, VHIGH\n")
        print ("====ERROR====", file=sys.stderr)
        sys.exit(-1)
    host = args[0]
    log_filename = args[1]
    memory_pressure_factor = args[2]
    lifetime_factor = args[3]
    request_factor = args[4]
    concurrent_session_factor = args[5]
    destroy_password = bool(args[6]) if len(args) > 6 else False
    use_tlslite= True


    jclientExp = JavaClientExperiments(host, log_filename, memory_pressure_factor, lifetime_factor, request_factor, concurrent_session_factor, perform_as_loop=True, use_tlslite=use_tlslite)
    jclientExp.setDestroy_pass(destroy_password)
    #jclientExp.stdout("Error: "+str(ex))
    jclientExp.start()
    while jclientExp.isRunning():
        try:
            time.sleep(.500)
        except MemoryError, oom_e:
            jclientExp.stdout("Error: "+str(oom_e))
        except Exception, e:
            jclientExp.stdout("Error: "+str(e))

    jclientExp.stdout("====DONE====")
    try:
        time.sleep(60)
    except MemoryError, oom_e:
        jclientExp.stdout("Error: "+str(oom_e))
    except Exception, e:
        jclientExp.stdout("Error: "+str(e))

    return 0



if __name__ == "__main__":
    if len(sys.argv) < 7:
        print (sys.argv[0] + \
            "JavaClientExperiment <host> <log_dir_location> <log_filename> <memory_pressure_factor>"+\
            " <lifetime_factor>, <request_factor>, <concurrent_session_factor> [destroy_password]\n"+\
            "Allowed factor values are : VLOW, LOW, MED, HIGH, VHIGH\n")
        print ("====ERROR====", file=sys.stderr)
        sys.exit(-1)
    run_java_exp(*(sys.argv[1:]))

