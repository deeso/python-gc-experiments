from __future__ import print_function
__author__ = 'dso'
from datetime import datetime

import random, base64, time
import gc, pysize, sys

MAX_MEMORY_2560 = 2048*1024*1024
MAX_MEMORY_512 = 512*1024*1024
MAX_MEMORY = MAX_MEMORY_512
MEMORY_WATER_MARK = MAX_MEMORY*.80
MIN_CUSHION = MAX_MEMORY - MEMORY_WATER_MARK


class ParameterGeneration(object):
    instance = None
    def __init__(self):
        self.instance = self

    def getTime(self):
        return long(time.mktime(datetime.now().timetuple()));

    @staticmethod
    def getinstance():
        if ParameterGeneration.instance is None:
            ParameterGeneration.instance = ParameterGeneration()
        return ParameterGeneration.instance

    def base64Encode(self, string):
        return base64.encodestring(string).strip()

    def getBigIntBytes(self, bits):
        num_bytes = bits/8
        _bytes = []
        for i in xrange(0, num_bytes):
             _bytes.append(chr(random.getrandbits(8)))
        return "".join(_bytes)

    def nextSessionId(self):
        return self.base64Encode(self.getBigIntBytes(240))

    def nextUserId(self):
        return self.base64Encode(self.getBigIntBytes(80))

    def nextPasswordId(self):
        return self.base64Encode(self.getBigIntBytes(120))

    def randomBytes(self, sz):
        return self.getBigIntBytes(sz*8)

    def randomInt(self, max):
        v = random.random() * max
        return 1 if v < 1.0 else v


    def randomDouble(self, min=sys.float_info.min, max=sys.float_info.max):
        return self.randomFloat(min, max)

    def randomFloat(self, min=sys.float_info.min, max=sys.float_info.max):
        v = random.random() * max
        return min if v < min else v

    def coinFlip(self):
        return random.randint(0, 1) == 1

    def randomPercent(self, min=.01, max=1.0):
        return random.random() * 100

    def getTotalUsedMemory(self):
        import os
        import psutil
        process = psutil.Process(os.getpid())
        return process.memory_info()[0]
        #print ("Calling get total used memory")
        #return pysize.asizeof(gc.get_objects())

    def getUsedMemory(self, obj):
        return pysize.asizeof(obj)

    def getFreeMemory(self):
        print ("Calling get free memory")
        return self.totalMemory() - self.getTotalUsedMemory()

    def totalMemory(self):
        return MEMORY_WATER_MARK