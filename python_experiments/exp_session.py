from __future__ import print_function
__author__ = 'dso'
import cookielib, urllib, sys, ssl, socket, tlslite
import httplib as http
from param_gen import ParameterGeneration
from events import MyTimer, PerformLogin, PerformMediumReq, PerformSmallReq
import Cookie
import threading

ssl._create_default_https_context = ssl._create_unverified_context

class ExperimentSession(object):
    BLOCK_SIZE = 512*4*8
    SESSIONID_NAME = "THESESSIONID"
    LOGIN_URL = "/login.html"
    MED_URL = "/htmlfile_med.html"
    SMALL_URL = "/htmlfile_small.html"
    base_size = None
    gjclientexp = None
    def __init__(self, jclientExp):
        self.gjclientexp = jclientExp if self.gjclientexp is None else self.gjclientexp
        self.scheduler = MyTimer(self)
        self.host = jclientExp.getRemoteHost()
        self.sessionId = ParameterGeneration.getinstance().nextSessionId()
        self.cookie = Cookie.SimpleCookie()
        self.addCookie(self.SESSIONID_NAME, self.sessionId, "." + self.host, "/")
        self.sessLifetime = 0
        self.memConsume = 0
        self.error = False
        self.alive = False
        self.shutdown = False
        self.runner = None
        #self.task_complete_lock = threading.RLock()
        self.expired = False

        self.completed = False
        self.dataGramSender = None
        self.completedSteps = 0
        self.task = None
        self.startTime = ParameterGeneration.getinstance().getTime()

        self.usingBouncyCastle = False
        self.wipe = False
        self.user = ParameterGeneration.getinstance().nextUserId()
        self.password = ParameterGeneration.getinstance().nextPasswordId()

        self.allowedRequests = int(jclientExp.getAllowedRequests())
        self.memConsume = jclientExp.getMemoryPerSession()
        self.sessLifetime = jclientExp.getSessLifetime()
        if self.allowedRequests == 0:
            self.allowedRequests = 100

        self.timerSpacing = self.sessLifetime / float(self.allowedRequests ) #* 1000
        self.perReqConsume = self.memConsume / self.allowedRequests


        self.data = ""

        self.maxed = False
        self.reportedMaxedOut = False

        #self.buildClient()
        self.MAX_SIZE = 0
        self.MIN_SIZE = self.baseSize()
        self.use_tlslite = jclientExp.useTlslite()
        self.perform_as_loop = jclientExp.perform_as_loop

    def getRemoteHost(self):
        return self.host

    def get_parameters(self):
        return urllib.urlencode({'user': self.user, 'password':self.password})

    def buildClient(self):
        if not self.use_tlslite:
            ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
            ssl_context.verify_mode = ssl.CERT_NONE
            #client = http.HTTPSConnection(self.getRemoteHost(), port=443, context=ssl_context)
            client = http.HTTPSConnection(self.getRemoteHost())
            client.set_debuglevel(0)
        else:
            client = tlslite.HTTPTLSConnection(self.getRemoteHost(), 443)
        return client

    def baseSize(self):
        if ExperimentSession.base_size is None:
            ExperimentSession.base_size = ParameterGeneration.getinstance().getUsedMemory(self)
        return ExperimentSession.base_size

    def cancelTimer(self):
        try:
            self.task.cancel()
            self.scheduler.cancel()
        except Exception, ex:
            pass

    def performAsLoop(self):
        loginTask = PerformLogin(self, self.timerSpacing, method="POST", url=self.login_url(), body=self.get_parameters() ,headers=self.getHttpCookie(), start_timer=False)
        loginTask.execute_task()
        while True:
            if self.alive and not self.error and not self.completed:
                if ParameterGeneration.getinstance().coinFlip():
                    task = PerformMediumReq(self, self.timerSpacing, method="GET", url=self.small_url(), body=None, headers=self.getHttpCookie(), start_timer=False)
                    task.execute_task()
                else:
                    task = PerformSmallReq(self, self.timerSpacing, method="GET", url=self.small_url(), body=None, headers=self.getHttpCookie(), start_timer=False)
                    task.execute_task()
            elif not self.completed and self.error:
                self.completed = False
                self.alive = False
                self.gjclientexp.handleFailedSession(self.getExpID())
            else:
                break


    def start(self):
        self.MIN_SIZE = self.baseSize()
        self.startTime = ParameterGeneration.getinstance().getTime()
        if not self.performAsLoop:
            self.scheduler.start(self.sessLifetime)
            self.scheduler.schedule(PerformLogin, 0, "POST", self.login_url(), self.get_parameters(), self.getHttpCookie())
        else:
            #print ("Executing as a loop")
            self.runner = threading.Thread(target=self.performAsLoop, args=())
            self.runner.start()
        self.alive = True
        self.completed = False

    def calculateSize(self):
        sz = 0 if self.data is None else len(self.data)
        return self.baseSize() + sz

    def schedule_next_task(self, seconds=None):

        if self.alive and not self.error and not self.completed:

            if ParameterGeneration.getinstance().coinFlip():
                self.scheduler.schedule(PerformMediumReq, seconds, "GET", self.med_url(), None, self.getHttpCookie())
            else:
                self.scheduler.schedule(PerformSmallReq, seconds, "GET", self.small_url(), None, self.getHttpCookie())

        elif not self.completed and self.error:
            self.completed = False
            self.alive = False
            self.cancelTimer()
            self.gjclientexp.handleFailedSession(self.getExpID())


    def setDataBlock(self, memCon):
        if memCon > 0 and memCon % 32 > 0:
            memCon += (memCon%32)
        elif memCon == 0:
            memCon = 32

        new_segs = []
        d = "{:04x}_{}_b4ds3cr3tsh3r3".format(self.completedSteps,self.user)
        new_segs.append(self.data)
        incr = len(d)
        c_len = len(self.data)
        new_len = c_len + memCon
        #print ("allocating %d bytes"%new_len)
        while c_len < new_len:
            new_segs.append(d)
            c_len += incr

        self.data = "".join(new_segs)
        #print ("idone allocating %d bytes"%c_len)

    def getDataSize(self):
        return len(self.data)

    def moreData(self, memCon=BLOCK_SIZE):
        if memCon < ExperimentSession.BLOCK_SIZE:
            self.setDataBlock(ExperimentSession.BLOCK_SIZE)
        else:
            self.setDataBlock(memCon)

    def aroundMemConsumption(self):
        size = self.getDataSize()
        expectedMemCon = self.getExpectedMemConsumption()
        return size >= expectedMemCon * .1 or size <= expectedMemCon * 1.1

    def getAdditionalPressureSize(self):
        emc = self.getExpectedMemConsumption()
        should_incr = self.perReqConsume
        max_ = self.gjclientexp.getMemoryPressureFactor()
        min_ = max_ - .2
        if (max_ < .2):
            min_ = 0.0

        fudge = ParameterGeneration.getinstance().randomPercent(min_, max_)
        if self.getDataSize() > emc:
            should_incr = 0
        elif ParameterGeneration.getinstance().coinFlip():
            should_incr += long(should_incr*fudge)

        return should_incr

    def getExpectedMemConsumption(self):
        return self.completedSteps * self.perReqConsume


    def sendDatagramPacket(self):
        if self.data is None or len(self.data):
            return

        if not self.dataGramSender is None and self.dataGramSender.isAlive():
            return

        try:
            sz = 8192 if len(self.data) > 8192 else len(self.data)
            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            sock.sendto(self.data[:sz], ("224.0.1.2", 49900))
        except MemoryError, oom_e:
            out = "Error: {}:{:04x} failed {:04x} memory allocation in sendDatagramPacket, {}\n".format(
                                            self.user, self.completedSteps, str(oom_e))
            self.do_stdout(out)
        self.data = ""

    def printCookieValues(self):
        #for cookie in self.cookieStore:
        #    print cookie.name, cookie.value
        pass

    def addCookie(self, name, value, domain, path):
        self.cookie[name] = value
        self.cookie[name]['domain'] = domain
        self.cookie[name]['path'] = path

    def getHttpCookie(self):
        name = self.SESSIONID_NAME
        return {"Cookie":"{}={}".format(name, self.cookie[name].value)}

    def getNextTaskTime(self):
        return self.timerSpacing

    def setAuthenticated(self):
        self.sessionId = ParameterGeneration.getinstance().nextSessionId()
        self.addCookie(self.SESSIONID_NAME, self.sessionId, "." + self.host, "/")
        self.printCookieValues()
        self.gjclientexp.logUserAuth("{:08x}".format(self.startTime), self.user, self.password, self.sessionId)

    def reportSize(self):
        size = self.calculateSize()
        if size > self.MAX_SIZE:
            self.MAX_SIZE = size

        time_ = ParameterGeneration.getinstance().getTime()
        smem = ParameterGeneration.getinstance().getTotalUsedMemory()
        is_completed = self.completed
        is_maxed = 1 if self.maxed else 0
        is_expired = 1 if self.expired else 0
        out = "Time:{:08x}-User:{}-min:{:08x}-mem:{:08x}-max:{:010x}-smem:{:010x}-maxed:{:02x}-expired:{:02x}-completed:{:02x}-steps:{:04x}"\
                .format(time_, self.user, self.MIN_SIZE, size, self.MAX_SIZE, smem, is_maxed, is_expired, is_completed,
                        self.completedSteps)
        self.gjclientexp.stdout(out)

    def updateMemoryConsumption(self):
        if not self.gjclientexp.allowedToAllocate():
            if not self.reportedMaxedOut and len(self.data) > 0:
                self.maxed = True
                self.reportedMaxedOut = True

            if self.data and len(self.data) > 0:
                num = ParameterGeneration.getinstance().randomInt(len(self.data))
                num = num if num > 0 else num
                self.data = self.data[len(self.data)-num-1:-1]
            else:
                self.data = ""
        else:
            # flip a coin, either shed it all or add some on
            if self.data is None or len(self.data) == 0:
                self.data = ""
                memCon = 3 * self.BLOCK_SIZE
                self.moreData(memCon)
            elif ParameterGeneration.getinstance().coinFlip():
                memCon = self.getAdditionalPressureSize()
                d = self.data
                try:
                    if memCon > 0:
                        self.moreData(memCon)
                except MemoryError, ex:
                    try:
                        out = "Error: {}:{:04x} failed {:04x} mem allocation\n".format(self.user, self.completedSteps, memCon)
                        self.gjclientexp.stdout(out)
                        self.moreData(self.BLOCK_SIZE)
                    except Exception, ex1:
                        out = "Error: {}:{:04x} failed {:04x}, {}".format(self.user, self.completedSteps, self.memCon, str(ex1))
                        self.gjclientexp.stdout(out)
                        self.data = d

                except Exception, ex:
                    try:
                        out = "Error: {}:{:04x} failed {:04x} mem allocation\n".format(self.user, self.completedSteps, memCon)
                        self.gjclientexp.stdout(out)
                        self.moreData(self.BLOCK_SIZE)
                    except Exception, ex1:
                        out = "Error: {}:{:04x} failed {:04x}, {}".format(self.user, self.completedSteps, self.memCon, str(ex1))
                        self.gjclientexp.stdout(out)
                        self.data = d
            else:
                #self.sendDatagramPacket()
                self.data = ""

    def concat(*arrays):
        res = []
        for i in arrays:
            res = res + i
        return res

    def toObjects(self, bytesPrim):
        return [i for i in bytesPrim]

    def isAlive(self):
        return self.alive

    def setAlive(self, alive):
        self.alive = alive

    def getCompleted(self):
        return self.completed

    def getExpID(self):
        return self.user

    def setShutdown(self, shutdown=True):
        self.shutdown = shutdown

    def login_url(self):
        return "https://{}/{}".format(self.getRemoteHost(), self.LOGIN_URL)

    def med_url(self):
        return "https://{}/{}".format(self.getRemoteHost(), self.MED_URL)

    def small_url(self):
        return "https://{}/{}".format(self.getRemoteHost(), self.SMALL_URL)

    def perform_task_complete(self):
        #self.task_complete_lock.acquire()
        self.updateMemoryConsumption()
        seconds = self.timerSpacing 
        self.completedSteps += 1
        end = ParameterGeneration.getinstance().getTime()
        current = end - self.startTime

        self.expired = current > self.sessLifetime
        self.completed = self.expired or self.completedSteps >= self.allowedRequests
        self.reportSize()
        if self.shutdown or self.expired or self.completed:
            self.alive = False
            self.completed = True
            self.shutdown = True
            self.cancelTimer()
            self.gjclientexp.handleCompleteSession(self.getExpID())

        if not self.perform_as_loop:
            self.schedule_next_task(seconds=seconds)
        #self.task_complete_lock.release()

    def do_stdout(self, out):
        self.gjclientexp.stdout(out)
        
