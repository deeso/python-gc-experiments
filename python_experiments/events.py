from __future__ import print_function
from param_gen import ParameterGeneration
from threading import Timer
import traceback, urllib2, ssl, time
from multiprocessing import Process, Queue

__author__ = 'dso'

def submit_request(q, client, method, url, body, headers):
    #print ("Submitting the request")
    client.request(method, url, body=body,headers=headers)
    #print ("getting the response")
    response = client.getresponse()
    #print ("reading thje respotnse")
    data = response.read()
    #print ("putting the data enqueue")
    #q.put(data)
    #client.close()
    return 0

    


class MyTimer(object):
    def __init__(self, expSess):
        self.expSess = expSess
        self.end = -1
        self.timer = None
        self.started = False

    def start(self, end_time):
        self.started = True
        self.timer = Timer(end_time, self.stop_it)
        self.timer.start()

    def schedule(self, task_class, seconds, method=None, url=None, body=None,headers=None):
        if self.started and self.expSess and not self.expSess.shutdown:
            #print ("Scheduling task for %fs to %s url"%(seconds, url))
            task_class(self.expSess, seconds, method=method, url=url, body=body,headers=headers)
            return True
        return False

    def cancel(self):
        if self.timer:
            try:
                self.timer.cancel()
            except:
                pass

    def stop_it(self):
        self.timer.cancel()
        self.expSess.setShutdown()
        self.expSess = None

    def baseSize(self):
        if self.base_size is None:
            self.base_size = ParameterGeneration.getinstance().getUsedMemory(self)
        return self.base_size

class BaseTask(object):
    def __init__(self, expSess, seconds, method=None, url=None, body=None,headers=None, start_timer=True):
        self.url = url
        self.method=method
        self.body=body
        self.headers=headers
        self.expSess = expSess
        self.task_timer = None
        self.seconds = seconds
        if start_timer:
            self.start_timer()

    def start_timer(self):
        self.task_timer = Timer(self.seconds, self.execute_task)
        self.expSess.task = self.task_timer
        self.expSess.task.start()
        
    def task_complete(self):
        self.expSess = None

    def execute_task(self):
        pass

    def check_cancel_none(self):
        self.expSess.task = None
        t = self.task_timer
        if not t is None:
            t.cancel()

class PerformLogin(BaseTask):
    def execute_task(self):
        self.check_cancel_none()
        try:
            self.task()
    
        except Exception, e:
            self.expSess.error = True
            out = "{} Error: {}:{:04x} failed login, ".format(str(e), self.expSess.user, self.expSess.completedSteps)
            self.expSess.do_stdout(out)
        #self.expSess.gjclientexp.stdout("Login task completed for {}".format(self.expSess.user))
        #self.task_complete()


    def task(self):
        client = self.expSess.buildClient()
        #client.request("POST", self.expSess.login_url(), body=params,headers=self.expSess.getHttpCookie())
        #response = client.getresponse()
        #data = response.read()
        q = Queue()
        p = Process(target=submit_request, args=(q, client, self.method, self.url, self.body, self.headers))
        p.start()
        p.join()
        client.close()
        self.expSess.setAuthenticated()
        self.expSess.perform_task_complete()


class PerformMediumReq(BaseTask):
    def execute_task(self):
        self.check_cancel_none()
        try:
            self.task()
        except Exception, e:
            self.expSess.error = True
            out = "{} Error: {}:{:04x} medium request, ".format(str(e), self.expSess.user, self.expSess.completedSteps)
            self.expSess.do_stdout(out)
        #self.expSess.gjclientexp.stdout("MedReq task completed for {}".format(self.expSess.user))
        #self.task_complete()

    def task(self):
        client = self.expSess.buildClient()
        #print ("built the client")
        q = Queue()
        p = Process(target=submit_request, args=(q, client, self.method, self.url, self.body, self.headers))
        p.start()
        #print("process started")
        while p.is_alive():
             time.sleep(.05)
        p.join()
        #print ("process completed client")
        client.close()
        #client.request("GET", self.expSess.med_url(), None, self.expSess.getHttpCookie())
        #response = client.getresponse()
        #op = response.read()
        self.expSess.perform_task_complete()

class PerformSmallReq(BaseTask):
    def execute_task(self):
        self.check_cancel_none()
        try:
            self.task()
        except Exception, e:
            self.expSess.error = True
            out = "{} Error: {}:{:04x} small request, ".format(str(e), self.expSess.user, self.expSess.completedSteps)
            traceback.print_exc()
            self.expSess.do_stdout(out)
        #self.expSess.gjclientexp.stdout("SmallReq task completed for {}".format(self.expSess.user))
        #self.task_complete()

    def task(self):
        client = self.expSess.buildClient()
        q = Queue()
        p = Process(target=submit_request, args=(q, client, self.method, self.url, self.body, self.headers))
        p.start()
        while p.is_alive():
             time.sleep(.05)
        p.join()
        client.close()
        #client.request("GET", self.expSess.small_url(), None, self.expSess.getHttpCookie())
        #response = client.getresponse()
        #op = response.read()
        self.expSess.perform_task_complete()
